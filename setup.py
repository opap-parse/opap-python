#!/usr/bin/python
# coding=utf-8
from setuptools import setup


def readme():
    with open('README.rst') as f:
        return f.read()

setup(name='opap-python',
      version='0.1',
      description='A parser written in python that works with OPAP API',
      long_description=readme(),
      keywords='funniest joke comedy flying circus',
      url='https://gitlab.com/opap-parse/opap-python/',
      author='Trash Devs',
      author_email='trashdevs@gmail.com',
      license='MIT',
      packages=['opap-python'],
      install_requires=[
          'requests',
      ],
      test_suite='nose.collector',
      tests_require=['nose', 'nose-cover3'],
      include_package_data=True,
      zip_safe=False)
