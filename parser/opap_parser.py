#!/usr/bin/python
# coding=utf-8
import requests
import datetime
import constants


class OpapParser(object):
    availableGames = constants.GAME_NAMES
    availableExtensions = constants.EXT_NAMES
    baseUrl = constants.BASE_URL

    def __init__(self, game, extension):
        """
        Constructor.

        :param game: string The game to get results for.
        :param extension: string The content type to request for.
        """
        if game not in OpapParser.availableGames:
            raise KeyError('{0} is not a valid game.'.format(game))

        if extension not in OpapParser.availableExtensions:
            raise KeyError('{0} is not a valid extension.'.format(extension))

        self.game = game
        self.extension = extension

    def get_last_draw(self):
        """
        Data for the most recent draw or competition of a game.

        :return: string The result of the last draw as a string.
        """
        url = (self.baseUrl + '{0}/last.{1}').format(self.game, self.extension)
        return self.__get_response(url)

    def get_specific_draw(self, draw_number):
        """
        Data for a specific draw or competition of a game.

        :param draw_number: int The draw or competition number. The competition number comprises of the year (e.g. 2013)
        followed by the week that the competition took place (e.g. 42, for the 42nd week of the year).
        :return: string The result of the specific draw as a string.
        """
        input_draw = str(draw_number)
        if not (input_draw.isdigit() and 5 <= len(input_draw) <= 6):
            raise ValueError('{0} is not valid draw number.'.format(draw_number))

        year = input_draw[:4]
        if not (year.isdigit() and len(year) == 4):
            raise ValueError('{0} does not contain a valid year.'.format(draw_number))

        week = input_draw[4:]
        if not (week.isdigit() and 1 <= int(week) <= 52):
            raise ValueError('{0} does not contain a valid week of the year.'.format(draw_number))

        url = (self.baseUrl + '{0}/{1}.{2}').format(
            self.game, draw_number, self.extension
        )
        return self.__get_response(url)

    def get_draw_by_date(self, date):
        """
        Data for draws or competitions of a specific date.

        :param date: int The desired date (dd-MM-yyyy).
        :return: string The result of the given draw as string.
        """
        input_date = str(date)
        if not len(input_date) == 10:
            raise ValueError('{0} is not a valid date format.'.format(date))

        try:
            datetime.datetime.strptime(date, '%d-%m-%Y')
        except ValueError:
            raise

        url = (self.baseUrl + '{0}/drawDate/{1}.{2}').format(
            self.game, date, self.extension
        )
        return self.__get_response(url)

    def __get_response(self, url):
        """
        Fetches the data from the given URL.

        :param url: string The url to fetch contents from.
        :return: string Either a JSON string or an XML string.
        """
        try:
            response = requests.get(url)
        except (requests.ConnectionError, requests.HTTPError):
            raise

        if self.extension == 'json':
            return response.json()
        return response.text
