#!/usr/bin/python
# coding=utf-8

BASE_URL = 'http://applications.opap.gr/DrawsRestServices/'

GAME_NAMES = [
    'kino', 'lotto', 'joker', 'proto', 'super3', 'extra5', 'propogoal',
    'penalties', 'bowling', 'pοwerspin', 'proposun', 'proposat', 'propowed'
]

EXT_NAMES = [
    'json', 'xml'
]
