#!/usr/bin/python
# coding=utf-8
from opap_parser import OpapParser


pyopap = OpapParser('kino', 'json')
kino_last = pyopap.get_last_draw()
print kino_last
kino_by_date = pyopap.get_draw_by_date('13-01-2008')
print kino_by_date
kino_by_draw = pyopap.get_specific_draw(201513)
print kino_by_draw
